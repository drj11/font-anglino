# Anglino

from my sketch book 2022-12-01

A font drawn with an x-height of 20mm  and an ascender height of
30 mm, and a descender line (below baseline) of -10mm.

Base on a hexagonl o:

      _
     / ]
     [_/

the width of the **o** being 18mm

This translates to a design grid:

- 900 ascender line
- 600 ex-line
- 0 baseline
- -300 descender line
- 880 cap line

The UPEM is 1200 (slightly unusual).

Effectively giving a zoom of 30FDU/mm.
Making the 18mm wide **o** be 540FDU wide.

The monoline stroke is 40 FDU thick, with no optical adjustment
for horizontal vs vertical.


## Anglipen

A connected script version, vaguely following the D‘Nealian
model https://en.wikipedia.org/wiki/D%27Nealian

# END
